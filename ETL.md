# Extracting, Transforming, and Loading OSM Data into a Tile Server
OpenStreetMap data encompasses a broad and deep collection of geographic data and metadata. Only a small fraction of this data is needed to be able to construct the tileset necessary for this use case. However, several processing steps requiring substantial (but temporary) disk space are needed. This document describes how to filter canonical OpenStreetMap data down to only the data required for the Public Transit Focus use case. This documentation uses Belgium and its neighboring countries as an example area for the deployment of this tileset to exemplify the combination of several smaller OpenStreetMap region exports, but of course with enough resources, this tileset could be deployed for the entire world.

## Prerequisites
### Software
Using [this tutorial](https://journocode.com/en/tutorials/extracting-geodata-from-openstreetmap-with-osmfilter) as a reference, we see there are several OpenStreetMap command-line tools/libraries required to perform the needed data operations. These Linux-based software packages don't require any special sources (they are available among the default repositories included with Ubuntu, for instance):
* `osm2pgsql`
* `osmctools`
* `osmium-tool`
* `osmosis`

### Disk Space
A considerable amount of disk space is needed for this operation, and may exceed the disk capacity of some workstations. It is advised to perform proof-of-concept using an intentionally small region (e.g. small US states like Rhode Island, or small countries like Luxembourg and Belgium) and Docker on a workstation before proceeding to a server environment.

Sufficient disk space is needed to hold each compressed OpenStreetMap data extract (.pbf files) plus their uncompressed (.osm files) form, in addition to the merged uncompressed files. In other words, the space required to hold the uncompressed and unmerged files must be doubled to accommodate the additional space occupied by the monolothic merged file. The space occupied by the filtered final output is also required. 

In concrete terms, each *uncompressed* .osm file is roughly 20x larger than its coresponding .pbf file. If we use the OpenStreetMap data for Belgium and Luxembourg on [Geofabrik](https://download.geofabrik.de/europe.html) as an example, we can see the .pbf files are 473MB and 36MB, respectively. The .osm extracts (more detail in subsequent sections of this document) of these files are 9.33GB and 634MB, respectively. The only way to combine these files is in .osm format; Therefore, the combining operation requires *double* the disk space sufficient to hold all .osm files (the originals + all combined). In the case of Belgium + Luxembourg, this is 93GB. Finally, this combined file is filtered down (e.g. data like OpenStreetMap edit history and unnecessary features such as shops and hiking trails is removed). The end result is a significantly (\~80%) smaller file size than the original compressed files (in the case of Belgium + Luxembourg, it is 105MB filtered vs. their original combined and compressed size of \~509MB), but of course significant temporary storage space was still nonetheless required to achieve the end result. With this proof of concept, we can roughly predict the disk space required for Belgium *and* all its direct land neighbors based on their original .pbf file sizes from Geofabrik: 

* Belgium: 473MB
* France: 3.9GB
* Germany: 3.6GB
* Luxembourg: 36.2MB
* Netherlands: 1.1GB
* Sum of .pbf: 9.1GB
* Sum of .osm Extracts: ~9.1GB \* 20 = 182GB
* Merge of .osm files: 182GB
* Compressed extract: 9.1 / 5 = ~1.82GB
* **Grand Total: 9.1GB + 182GB + 182GB + 1.82GB = 375GB**

### Computing Resources
Tile generation is hardly expected to be urgent in this context, and therefore CPU/memory performance is not of utmost importance. This series of processing steps is likely to be completely automated on a periodic (perhaps only as frequently as quarterly, if not yearly) basis -- unless ad-hoc updates are required. 

**All the operations described in this document *may* be performed on the same (virtual) hardware as the actual tile serving/hosting, but because the steps described in this document are only needed temporarily and likely infrequently, __using computing resources (and storage resources in particular) that are allocated temporarily (and specifically for this task) is recommended. Any standard LTS Ubuntu server image is sufficient for the task.__**

## Extracting
The first step is downloading OpenStreetMap data in .pbf format. Because the area represented by the tiles is not nearly as large as Europe (but broader than just Belgium); all of the data from Belgium itself and its direct neighbors (by land) must be downloaded in .pbf format from the [Europe section of Geofabrik](https://download.geofabrik.de/europe.html). Direct links are included here for convenience:
* [Belgium](https://download.geofabrik.de/europe/belgium-latest.osm.pbf)
* [France](https://download.geofabrik.de/europe/france-latest.osm.pbf)
* [Germany](https://download.geofabrik.de/europe/germany-latest.osm.pbf)
* [Luxembourg](https://download.geofabrik.de/europe/luxembourg-latest.osm.pbf)
* [Netherlands](https://download.geofabrik.de/europe/netherlands-latest.osm.pbf)

These pbf files are extracts of live OpenStreetMap data, and are updated daily; however, the "freshness" of this particular map data is not a major concern for this use case as the geography of interest to public transit would not normally experience significant changes within days or weeks.

## Transforming
Transforming the OpenStreetMap data primarily involves using a series of command-line geographic processing tools, but most of the difficulty in this part of the process requires a deeper understanding of data filtering: ***Which OpenStreetMap data must be kept, and which may be filtered out?* This section seeks to clarify.**

### Filtering
Tiles are generated from three [elements](https://wiki.openstreetmap.org/wiki/Elements) (or types) of OpenStreetMap data (points, ways, and relations), and their respective metadata. 
#### Geographic Data
The geographic data is basically points, lines, and polygons (and related sets of each); each with unique IDs. Elements only represent the physical shape and location of features - not style, nor any other metadata. Geographic data may be filtered in terms of its geography, but in this use case, the geographic area has already been filtered to Belgium and its surrounding countries. The additional filtering will remove unnecessary geographic features (features completely unrelated to public transit) within the selected countries - not in terms of where those unnecessary features are or what their *shape* may be, but what *kinds* of features they are: in otheir words, filtering them based on their metadata.
#### Metadata
OpenStreetMap metadata such as [tags](https://wiki.openstreetmap.org/wiki/Tags) classify each element in plain language (e.g. an amenity point with the type restaurant; a highway polyline with the type motorway; an area of land whose landuse is forest). 
##### Overpass Turbo
OpenStreetMap's metadata schema is too rich and complex to understand how its tag filtering behaves without some visual feedback. The quickest and most convenient way to experiment with metadata and tag-based filtering on a map is to use a [web-based tool for Overpass Turbo](https://overpass-turbo.eu), which is documented [here](https://wiki.openstreetmap.org/wiki/Overpass_turbo). This tool is indispensible for quick trial-and-error filtering queries on OpenStreetMap metadata to highlight only the features returned by a filter query.  The following Overpass Turbo filter logic is a great starting point to copy and paste into the wizard, and is formatted nearly identically to the command-line filter logic used in a later step:
	
	“(amenity=railway_level_crossing or amenity=railway_crossing or amenity=landuse_railway) or bridge=* or boundary=administrative or (highway=primary or highway=secondary or highway=tertiary or highway=residential) or place=* or railway=* or route=* or tunnel=* or water=* or waterway=*”

In plain language, this filter only shows the following (and excludes everything else):
* Amenities whose type is any of the following: railway level crossing, railway crossing, or land used specifically for railways
* Bridges
* Administrative boundaries
* Primary, secondary, tertiary, and residential roadways
* Place names
* Railways
* Route names
* Tunnels
* Water bodies
* Waterways

This filter logic is not the final logic used, but is instead a good reference for understanding how to build OpenStreetMap filters. **PROTIP: The performance/speed of the Overpass Turbo tool reduces as the number of features on the map increases, so zooming the map in as close as possible to some feature types of interest is recommended to minimize the number of features to be processed in the map view**
##### Osmfilter
The real filtering tool used in this process is a command-line utility called [`osmfilter`](https://wiki.openstreetmap.org/wiki/Osmfilter). It uses very similar syntax and logic to Overpass Turbo, but has no visual tool conveniently associated with it. Therefore, if a particular filter setting is not yet known to be desirable, using a very restricted geographic area (such as the [Flevoland region of the Netherlands](https://download.geofabrik.de/europe/netherlands/flevoland.html)) for trial-and-error testing would save substantial time over using country-wide data (or alternatively, of course, simply use Overpass Turbo). The filtering and loading process is quite time consuming (at least several minutes) regardless of how simple the map filter setting is, so anything to reduce the amount of data to be filtered and loaded reduces the time to test settings.

The complete `osmfilter` command used for the Public Transit Focus map is:
	
	osmfilter belgiumandneighbors.osm --keep="admin_level=2 amenity=railway_level_crossing =railway_crossing =landuse_railway bridge= boundary=administrative highway= place= railway= route= tunnel= water= waterway=" --drop="route=hiking" --drop-ways="place=" --drop-relations="place=" --drop-author --drop-version -o=belgiumandneighbors.o5m

This expands on the previous Overpass Turbo settings in a few notable ways:
* `"[admin_level](https://wiki.openstreetmap.org/wiki/Key:admin_level)=2"` - This indicates the display of country-level administrative borders, and no smaller administrative subdivisions
* `--drop="route=hiking"` - Any routes with the tag value "hiking" are filtered out
* `--drop-ways="place="` and `--drop-relations="place="` - This is not an intuitive filter, but very important. This means that all places (such as cities and towns) should only be represented (and labeled) as points; not as ways, areas, or collections of multiple OpenStreetMap element types. If this filter is not used, many borders (particularly of large geographic areas for which a municipality or a region is responsible) are labeled, and clutter the map with information useless to this use case.
* `--drop-author` and `--drop-version` - These are massively beneficial flags to pass in the osmfilter command, as they save a substantial amount of disk space by stripping out author (the open source community contributors of map data on OpenStreetMap) metadata and versioned change history of each OpenStreetMap feature. This use case does not use this type of information, so filtering it out saves quite some storage space (and inevitably increases performance).

## Loading
This final step puts all the pieces together from previous steps. Assuming there is now a directory containing 5 .pbf files repressenting Begium and its adjacent neighboring countries, they must each be converted (using the `osmconvert` utility) to .osm files:

	for pbf in *.pbf; do osmconvert "$pbf" -o="$pbf".osm; done
Now, there are 5 .osm files in addition to 5 new .pbf files. *Note: The .pbf files are now safe to delete if the processing environment itself is not ephemeral by design and to be entirely deleted immediately after processing anyway.* These 5 .osm files must now be merged into one **huge** .osm file using `osmium`:

	osmium merge *.osm -o belgiumandneighbors.osm
Next, the merged .osm file has all unneeded features filtered from it using `osmfilter`:

	osmfilter belgiumandneighbors.osm --keep="admin_level=2 amenity=railway_level_crossing =railway_crossing =landuse_railway bridge= boundary=administrative highway= place= railway= route= tunnel= water= waterway=" --drop="route=hiking" --drop-ways="place=" --drop-relations="place=" --drop-author --drop-version -o=belgiumandneighbors.o5m
Finally, use `osmconvert` to convert the .o5m file back to a pbf file `data.osm.pbf` (which is the default file name and format expected by the Docker container), and place it within the publictransitfocus directory.

	osmconvert belgiumandneighbors.o5m -o=data.osm.pbf

Now that the data is ready in the expected format, it may be loaded into the PostGIS database manually (the `data.osm.pbf` generated in this document should be used whenever [INSTALL.md](https://gitlab.com/thoughtful_explorer/publictransitfocus/-/blob/master/INSTALL.md) references `data.osm.pbf`), or automatically (if [DOCKER.md]https://gitlab.com/thoughtful_explorer/publictransitfocus/-/blob/master/DOCKER.md) is being used).
