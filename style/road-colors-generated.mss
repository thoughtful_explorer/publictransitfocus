/* This is generated code, do not change this file manually.          */
/*                                                                    */
/* To change these definitions, alter road-colors.yaml and run:       */
/*                                                                    */
/* scripts/generate_road_colours.py > style/road-colors-generated.mss */
/*                                                                    */
@motorway-casing: #fcfcfc;
@trunk-casing: #fcfcfc;
@primary-casing: #fcfcfc;
@secondary-casing: #fcfcfc;
@motorway-fill: #fcfcfc;
@trunk-fill: #fcfcfc;
@primary-fill: #fcfcfc;
@secondary-fill: #fcfcfc;
@motorway-low-zoom: #fcfcfc;
@trunk-low-zoom: #fcfcfc;
@primary-low-zoom: #fcfcfc;
@secondary-low-zoom: #fcfcfc;
@motorway-low-zoom-casing: #fcfcfc;
@trunk-low-zoom-casing: #fcfcfc;
@primary-low-zoom-casing: #fcfcfc;
@secondary-low-zoom-casing: #fcfcfc;
