/* For the main linear features, such as roads and railways. */

@tertiary-fill: #ffffff;
@residential-fill: #ffffff;
@service-fill: @residential-fill;
@living-street-fill: #fff;
@pedestrian-fill: #fff;
@raceway-fill: #fff;
@road-fill: #fff;
@access-marking: #fff;
@access-marking-living-street: #fff;

@default-casing: white;
@tertiary-casing: #fff;
@residential-casing: #fff;
@road-casing: @residential-casing;
@service-casing: @residential-casing;
@living-street-casing: @residential-casing;
@pedestrian-casing: #fff;

@minor-construction: #aaa;
@service-construction: #aaa;

@destination-marking: #c2e0ff;

@tunnel-casing: grey;
@bridge-casing: #fff;

@motorway-tunnel-fill: lighten(@motorway-fill, 10%);
@trunk-tunnel-fill: lighten(@trunk-fill, 10%);
@primary-tunnel-fill: lighten(@primary-fill, 10%);
@secondary-tunnel-fill: lighten(@secondary-fill, 5%);
@tertiary-tunnel-fill: lighten(@tertiary-fill, 5%);
@residential-tunnel-fill: darken(@residential-fill, 5%);
@living-street-tunnel-fill: lighten(@living-street-fill, 3%);

@motorway-width-z6:               0.4;
@trunk-width-z6:                  0.4;

@motorway-width-z7:               0.8;
@trunk-width-z7:                  0.6;

@motorway-width-z8:               1;
@trunk-width-z8:                  1;
@primary-width-z8:                1;

@motorway-width-z9:               1.4;
@trunk-width-z9:                  1.4;
@primary-width-z9:                1.4;

@motorway-width-z10:              1.9;
@trunk-width-z10:                 1.9;
@primary-width-z10:               1.8;

@motorway-width-z11:              2.0;
@trunk-width-z11:                 1.9;
@primary-width-z11:               1.8;
@secondary-width-z11:             1.1;

@motorway-width-z12:              3.5;
@motorway-link-width-z12:         1.5;
@trunk-width-z12:                 3.5;
@trunk-link-width-z12:            1.5;
@primary-width-z12:               3.5;
@primary-link-width-z12:          1.5;
@secondary-width-z12:             3.5;
@secondary-link-width-z12:        1.5;

@motorway-width-z13:              6;
@motorway-link-width-z13:         4;
@trunk-width-z13:                 6;
@trunk-link-width-z13:             4;
@primary-width-z13:               5;
@primary-link-width-z13:          4;
@secondary-width-z13:             5;
@secondary-link-width-z13:        4;

@secondary-width-z14:             5;

@service-width-z15:                2;
@road-width-z15:                  3;
@motorway-width-z15:             10;
@motorway-link-width-z15:         7.8;
@trunk-width-z15:                10;
@trunk-link-width-z15:          7.8;
@primary-width-z15:              10;
@primary-link-width-z15:        7.8;
@secondary-width-z15:             9;
@secondary-link-width-z15:        7;
@tertiary-width-z15:              9;
@tertiary-link-width-z15:         7;
@residential-width-z15:           5;
@living-street-width-z15:         5;
@pedestrian-width-z15:            5;

@secondary-width-z16:            10;
@tertiary-width-z16:             10;
@residential-width-z16:           6;
@living-street-width-z16:         6;
@pedestrian-width-z16:            6;
@road-width-z16:                  3.5;
@service-width-z16:               3.5;
@minor-service-width-z16:         2;

@motorway-width-z17:             18;
@motorway-link-width-z17:        12;
@trunk-width-z17:                18;
@trunk-link-width-z17:           12;
@primary-width-z17:              18;
@primary-link-width-z17:         12;
@secondary-width-z17:            18;
@secondary-link-width-z17:       12;
@tertiary-width-z17:             18;
@tertiary-link-width-z17:        12;
@residential-width-z17:          12;
@living-street-width-z17:        12;
@pedestrian-width-z17:           12;
@road-width-z17:                  7;
@service-width-z17:               7;
@minor-service-width-z17:         3.5;

@motorway-width-z18:             21;
@motorway-link-width-z18:        13;
@trunk-width-z18:                21;
@trunk-link-width-z18:           13;
@primary-width-z18:              21;
@primary-link-width-z18:         13;
@secondary-width-z18:            21;
@secondary-link-width-z18:       13;
@tertiary-width-z18:             21;
@tertiary-link-width-z18:        13;
@residential-width-z18:          13;
@living-street-width-z18:        13;
@pedestrian-width-z18:           13;
@road-width-z18:                  8.5;
@service-width-z18:               8.5;
@minor-service-width-z18:         4.75;

@motorway-width-z19:             27;
@motorway-link-width-z19:        16;
@trunk-width-z19:                27;
@trunk-link-width-z19:           16;
@primary-width-z19:              27;
@primary-link-width-z19:         16;
@secondary-width-z19:            27;
@secondary-link-width-z19:       16;
@tertiary-width-z19:             27;
@tertiary-link-width-z19:        16;
@residential-width-z19:          17;
@living-street-width-z19:        17;
@pedestrian-width-z19:           17;
@road-width-z19:                 11;
@service-width-z19:              11;
@minor-service-width-z19:         5.5;

@motorway-width-z20:             33;
@motorway-link-width-z20:        17;
@service-width-z20:              12;
@minor-service-width-z20:         8.5;


@major-casing-width-z11:          0.3;

@casing-width-z12:                0.3;
@secondary-casing-width-z12:      0.3;
@major-casing-width-z12:          0.5;

@casing-width-z13:                0.5;
@residential-casing-width-z13:    0.5;
@secondary-casing-width-z13:      0.35;
@major-casing-width-z13:          0.5;

@casing-width-z14:                0.55;
@secondary-casing-width-z14:      0.35;
@major-casing-width-z14:          0.6;

@casing-width-z15:                0.6;
@secondary-casing-width-z15:      0.7;
@major-casing-width-z15:          0.7;

@casing-width-z16:                0.6;
@secondary-casing-width-z16:      0.7;
@major-casing-width-z16:          0.7;

@casing-width-z17:                0.8;
@secondary-casing-width-z17:      1;
@major-casing-width-z17:          1;

@casing-width-z18:                0.8;
@secondary-casing-width-z18:      1;
@major-casing-width-z18:          1;

@casing-width-z19:                0.8;
@secondary-casing-width-z19:      1;
@major-casing-width-z19:          1;

@casing-width-z20:                0.8;
@secondary-casing-width-z20:      1;
@major-casing-width-z20:          1;

@bridge-casing-width-z12:         0.1;
@major-bridge-casing-width-z12:   0.5;
@bridge-casing-width-z13:         0.5;
@major-bridge-casing-width-z13:   0.5;
@bridge-casing-width-z14:         0.5;
@major-bridge-casing-width-z14:   0.6;
@bridge-casing-width-z15:         0.75;
@major-bridge-casing-width-z15:   0.75;
@bridge-casing-width-z16:         0.75;
@major-bridge-casing-width-z16:   0.75;
@bridge-casing-width-z17:         0.8;
@major-bridge-casing-width-z17:   1;
@bridge-casing-width-z18:         0.8;
@major-bridge-casing-width-z18:   1;
@bridge-casing-width-z19:         0.8;
@major-bridge-casing-width-z19:   1;
@bridge-casing-width-z20:         0.8;
@major-bridge-casing-width-z20:   1;

@paths-background-width:          1;
@paths-bridge-casing-width:       0.5;
@paths-tunnel-casing-width:       1;

@mini-roundabout-width:           4;

@junction-text-color:             #960000;
@halo-color-for-minor-road:       white;
@lowzoom-halo-color:              white;
@lowzoom-halo-width:              1;

@motorway-oneway-arrow-color:     darken(@motorway-casing, 25%);
@trunk-oneway-arrow-color:        darken(@trunk-casing, 25%);
@primary-oneway-arrow-color:      darken(@primary-casing, 15%);
@secondary-oneway-arrow-color:    darken(@secondary-casing, 10%);
@tertiary-oneway-arrow-color:     darken(@tertiary-casing, 30%);
@residential-oneway-arrow-color:  darken(@residential-casing, 40%);
@living-street-oneway-arrow-color: darken(@residential-casing, 30%);
@pedestrian-oneway-arrow-color:   darken(@pedestrian-casing, 25%);
@raceway-oneway-arrow-color:      darken(@raceway-fill, 50%);

@major-highway-text-repeat-distance: 50;
@minor-highway-text-repeat-distance: 10;

@railway-text-repeat-distance: 200;

#roads-casing, #bridges, #tunnels {
  ::casing {
    [zoom >= 12] {
      [feature = 'highway_motorway'] {
        line-width: @motorway-width-z12;
        [zoom >= 13] { line-width: @motorway-width-z13; }
        [zoom >= 15] { line-width: @motorway-width-z15; }
        [zoom >= 17] { line-width: @motorway-width-z17; }
        [zoom >= 18] { line-width: @motorway-width-z18; }
        [zoom >= 19] { line-width: @motorway-width-z19; }
        [zoom >= 20] { line-width: @motorway-width-z20; }
        [link = 'yes'] {
          line-width: @motorway-link-width-z12;
          [zoom >= 13] { line-width: @motorway-link-width-z13; }
          [zoom >= 15] { line-width: @motorway-link-width-z15; }
          [zoom >= 17] { line-width: @motorway-link-width-z17; }
          [zoom >= 18] { line-width: @motorway-link-width-z18; }
          [zoom >= 19] { line-width: @motorway-link-width-z19; }
          [zoom >= 20] { line-width: @motorway-link-width-z20; }
        }
        line-color: @motorway-low-zoom-casing;
        [zoom >= 13] {
          line-color: @motorway-casing;
        }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          line-join: round;
          [zoom >= 13] { line-color: @bridge-casing; }
        }
      }
    }

    [feature = 'highway_trunk'] {
      [zoom >= 12] {
        line-color: @trunk-low-zoom-casing;
        [zoom >= 13] {
          line-color: @trunk-casing;
        }
        line-width: @trunk-width-z12;
        [zoom >= 13] { line-width: @trunk-width-z13; }
        [zoom >= 15] { line-width: @trunk-width-z15; }
        [zoom >= 17] { line-width: @trunk-width-z17; }
        [zoom >= 18] { line-width: @trunk-width-z18; }
        [zoom >= 19] { line-width: @trunk-width-z19; }
        [link = 'yes'] {
          line-width: @trunk-link-width-z12;
          [zoom >= 13] { line-width: @trunk-link-width-z13; }
          [zoom >= 15] { line-width: @trunk-link-width-z15; }
          [zoom >= 17] { line-width: @trunk-link-width-z17; }
          [zoom >= 18] { line-width: @trunk-link-width-z18; }
          [zoom >= 19] { line-width: @trunk-link-width-z19; }
        }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          line-join: round;
          [zoom >= 13] { line-color: @bridge-casing; }
        }
      }
    }

    [feature = 'highway_primary'] {
      [zoom >= 12] {
        line-color: @primary-low-zoom-casing;
        [zoom >= 13] {
          line-color: @primary-casing;
        }
        line-width: @primary-width-z12;
        [zoom >= 13] { line-width: @primary-width-z13; }
        [zoom >= 15] { line-width: @primary-width-z15; }
        [zoom >= 17] { line-width: @primary-width-z17; }
        [zoom >= 18] { line-width: @primary-width-z18; }
        [zoom >= 19] { line-width: @primary-width-z19; }
        [link = 'yes'] {
          line-width: @primary-link-width-z12;
          [zoom >= 13] { line-width: @primary-link-width-z13; }
          [zoom >= 15] { line-width: @primary-link-width-z15; }
          [zoom >= 17] { line-width: @primary-link-width-z17; }
          [zoom >= 18] { line-width: @primary-link-width-z18; }
          [zoom >= 19] { line-width: @primary-link-width-z19; }
        }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          line-join: round;
          [zoom >= 15] { line-color: @bridge-casing; }
        }
      }
    }

    [feature = 'highway_secondary'] {
      [zoom >= 15] {
        line-color: @secondary-low-zoom-casing;
        line-width: @secondary-width-z15;
        [zoom >= 16] { line-width: @secondary-width-z16; }
        [zoom >= 17] { line-width: @secondary-width-z17; }
        [zoom >= 18] { line-width: @secondary-width-z18; }
        [zoom >= 19] { line-width: @secondary-width-z19; }
        [link = 'yes'] {
          line-width: @secondary-link-width-z15;
          [zoom >= 17] { line-width: @secondary-link-width-z17; }
          [zoom >= 18] { line-width: @secondary-link-width-z18; }
          [zoom >= 19] { line-width: @secondary-link-width-z19; }
        }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          [zoom >= 15] {
            line-color: @bridge-casing;
            line-join: round;
          }
        }
      }
    }

    [feature = 'highway_tertiary'] {
      [zoom >= 15] {
        line-color: @tertiary-casing;
        line-width: @tertiary-width-z15;
        [zoom >= 16] { line-width: @tertiary-width-z16; }
        [zoom >= 17] { line-width: @tertiary-width-z17; }
        [zoom >= 18] { line-width: @tertiary-width-z18; }
        [zoom >= 19] { line-width: @tertiary-width-z19; }
        [link = 'yes'] {
          line-width: @tertiary-link-width-z15;
          [zoom >= 17] { line-width: @tertiary-link-width-z17; }
          [zoom >= 18] { line-width: @tertiary-link-width-z18; }
          [zoom >= 19] { line-width: @tertiary-link-width-z19; }
        }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          [zoom >= 15] {
            line-color: @bridge-casing;
            line-join: round;
          }
        }
      }
    }

    [feature = 'highway_residential'],
    [feature = 'highway_unclassified'] {
      [zoom >= 15] {
        line-color: @residential-casing;
        line-width: @residential-width-z15;
        [zoom >= 16] { line-width: @residential-width-z16; }
        [zoom >= 17] { line-width: @residential-width-z17; }
        [zoom >= 18] { line-width: @residential-width-z18; }
        [zoom >= 19] { line-width: @residential-width-z19; }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          [zoom >= 15] {
            line-color: @bridge-casing;
            line-join: round;
          }
        }
      }
    }

    [feature = 'highway_road'] {
      [zoom >= 15] {
        line-color: @road-casing;
        line-width: @road-width-z15;
        [zoom >= 16] { line-width: @road-width-z16; }
        [zoom >= 17] { line-width: @road-width-z17; }
        [zoom >= 18] { line-width: @road-width-z18; }
        [zoom >= 19] { line-width: @road-width-z19; }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          line-color: @bridge-casing;
          line-join: round;
        }
      }
    }
  
    [feature = 'highway_service'] {
      [zoom >= 15][service = 'INT-normal'],
      [zoom >= 16][service = 'INT-minor'] {
        line-color: @service-casing;
        [service = 'INT-normal'] {
          line-width: @service-width-z15;
          [zoom >= 16] { line-width: @service-width-z16; }
          [zoom >= 17] { line-width: @service-width-z17; }
          [zoom >= 18] { line-width: @service-width-z18; }
          [zoom >= 19] { line-width: @service-width-z19; }
          [zoom >= 20] { line-width: @service-width-z20; }
        }
        [service = 'INT-minor'] {
          line-width: @minor-service-width-z16;
          [zoom >= 17] { line-width: @minor-service-width-z17; }
          [zoom >= 18] { line-width: @minor-service-width-z18; }
          [zoom >= 19] { line-width: @minor-service-width-z19; }
          [zoom >= 20] { line-width: @minor-service-width-z20; }
        }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          line-color: @bridge-casing;
          line-join: round;
        }
      }
    }

    [feature = 'highway_pedestrian'] {
      [zoom >= 15] {
        line-color: @pedestrian-casing;
        line-width: @pedestrian-width-z15;
        [zoom >= 16] { line-width: @pedestrian-width-z16; }
        [zoom >= 17] { line-width: @pedestrian-width-z17; }
        [zoom >= 18] { line-width: @pedestrian-width-z18; }
        [zoom >= 19] { line-width: @pedestrian-width-z19; }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          line-color: @bridge-casing;
          line-join: round;
        }
      }
    }

    [feature = 'highway_living_street'] {
      [zoom >= 15] {
        line-color: @living-street-casing;
        line-width: @living-street-width-z15;
        [zoom >= 16] { line-width: @living-street-width-z16; }
        [zoom >= 17] { line-width: @living-street-width-z17; }
        [zoom >= 18] { line-width: @living-street-width-z18; }
        [zoom >= 19] { line-width: @living-street-width-z19; }
        #roads-casing {
          line-cap: round;
          line-join: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          [zoom >= 15] {
            line-color: @bridge-casing;
            line-join: round;
          }
        }
      }
    }

    [feature = 'railway_tram'],
    [feature = 'railway_tram-service'][zoom >= 15] {
      #bridges {
        [zoom >= 15] {
          line-width: 5;
        }
        line-color: @bridge-casing;
        line-join: round;
      }
    }

    [feature = 'railway_subway'],
    [feature = 'railway_construction']['construction' = 'subway'] {
      #bridges {
        [zoom >= 14] {
          line-width: 5.5;
          line-color: @bridge-casing;
          line-join: round;
        }
      }
    }

    [feature = 'railway_light_rail'],
    [feature = 'railway_funicular'],
    [feature = 'railway_narrow_gauge'] {
      #bridges {
        [zoom >= 14] {
          line-width: 5.5;
          line-color: @bridge-casing;
          line-join: round;
        }
      }
    }

    [feature = 'railway_rail'],
    [feature = 'railway_preserved'],
    [feature = 'railway_monorail'][zoom >= 14] {
      #bridges {
        [zoom >= 13] {
          line-width: 6.5;
          line-color: @bridge-casing;
          line-join: round;
        }
      }
    }

    [feature = 'railway_INT-spur-siding-yard'] {
      #bridges {
        [zoom >= 13] {
          line-width: 5.7;
          line-color: @bridge-casing;
          line-join: round;
        }
      }
    }
  }

  ::bridges_and_tunnels_background {
    [feature = 'railway_rail'][zoom >= 13],
    [feature = 'railway_preserved'][zoom >= 13],
    [feature = 'railway_monorail'][zoom >= 14] {
      #bridges {
        line-width: 5;
        line-color: white;
        line-join: round;
      }
    }

    [feature = 'railway_INT-spur-siding-yard'] {
      #bridges {
        [zoom >= 13] {
          line-width: 4;
          line-color: white;
          line-join: round;
        }
      }
    }

    [feature = 'railway_disused'][zoom >= 15],
    [feature = 'railway_construction']['construction' != 'subway'],
    [feature = 'railway_miniature'][zoom >= 15],
    [feature = 'railway_INT-preserved-ssy'][zoom >= 14] {
      #bridges {
        [zoom >= 13] {
          line-width: 4.5;
          line-color: white;
          line-join: round;
        }
      }
    }

    [feature = 'railway_tram'],
    [feature = 'railway_tram-service'][zoom >= 15] {
      #bridges {
        [zoom >= 13] {
          line-width: 3;
          [zoom >= 15] {
            line-width: 4;
          }
          line-color: white;
        }
      }
    }

    [feature = 'railway_subway'],
    [feature = 'railway_construction']['construction' = 'subway'] {
      #bridges {
        [zoom >= 14] {
          line-width: 4;
          line-color: white;
          line-join: round;
        }
      }
    }

    [feature = 'railway_light_rail'],
    [feature = 'railway_funicular'],
    [feature = 'railway_narrow_gauge'] {
      #bridges {
        [zoom >= 14] {
          line-width: 4;
          line-color: white;
          line-join: round;
        }
      }
    }
  }
}

/* Data on z<10 comes from osm_planet_roads, data on z>=10 comes from
osm_planet_line. This is for performance reasons: osm_planet_roads contains less
data, and is thus faster. Chosen is for zoom level 10 as cut-off, because
tertiary is rendered from z10 and is not included in osm_planet_roads.
*/
#roads-low-zoom[zoom < 10],
#roads-fill[zoom >= 10],
#bridges[zoom >= 10],
#tunnels[zoom >= 10] {

  ::halo {
    [zoom = 9][feature = 'highway_secondary'] {
      line-color: @halo-color-for-minor-road;
      line-width: 2.2;
      line-opacity: 0.4;
      line-join: round;
      //Missing line-cap: round; is intentional. It would cause rendering glow multiple times in some places - what as result of partial transparency would cause differences in rendering
      //Also, bridges - including bridge casings - are rendered on top of roads. Enabling line-cap: round would result in glow from bridges rendered on top of road around bridges.
    }
    [zoom = 10][feature = 'highway_secondary'],
    [zoom = 11][feature = 'highway_secondary'] {
      line-color: @halo-color-for-minor-road;
      line-width: 2.7;
      line-opacity: 0.4;
      line-join: round;
      //Missing line-cap: round; is intentional. It would cause rendering glow multiple times in some places - what as result of partial transparency would cause differences in rendering
      //Also, bridges - including bridge casings - are rendered on top of roads. Enabling line-cap: round would result in glow from bridges rendered on top of road around bridges.
    }
    [zoom = 10][feature = 'highway_tertiary'],
    [zoom = 11][feature = 'highway_tertiary'],
    [zoom = 12][feature = 'highway_unclassified'] {
      line-color: @halo-color-for-minor-road;
      line-width: 2.2;
      line-opacity: 0.3;
      line-join: round;
      //Missing line-cap: round; is intentional. It would cause rendering glow multiple times in some places - what as result of partial transparency would cause differences in rendering
      //Also, bridges - including bridge casings are rendered on top of roads. Enabling line-cap: round would result in glow from bridges rendered on top of road around bridges.
    }
    [feature = 'highway_motorway'][link != 'yes'][zoom >= 6][zoom < 12],
    [feature = 'highway_motorway'][link = 'yes'][zoom >= 10][zoom < 12],
    [feature = 'highway_trunk'][link != 'yes'][zoom >= 6][zoom < 12],
    [feature = 'highway_trunk'][link = 'yes'][zoom >= 10][zoom < 12],
    [feature = 'highway_primary'][link != 'yes'][zoom >= 8][zoom < 12],
    [feature = 'highway_primary'][link = 'yes'][zoom >= 10][zoom < 12],
    [feature = 'highway_secondary'][zoom >= 11][zoom < 12] {
      [feature = 'highway_motorway'] {
        [zoom >= 6] { line-width: @motorway-width-z6 + 2 * @lowzoom-halo-width; }
        [zoom >= 7] { line-width: @motorway-width-z7 + 2 * @lowzoom-halo-width; }
        [zoom >= 8] { line-width: @motorway-width-z8 + 2 * @lowzoom-halo-width; }
        [zoom >= 9] { line-width: @motorway-width-z9 + 2 * @lowzoom-halo-width; }
        [zoom >= 10] { line-width: @motorway-width-z10 + 2 * @lowzoom-halo-width; }
        [zoom >= 11] { line-width: @motorway-width-z11 + 2 * @lowzoom-halo-width; }
      }
      [feature = 'highway_trunk'] {
        [zoom >= 6] { line-width: @trunk-width-z6 + 2 * @lowzoom-halo-width; }
        [zoom >= 7] { line-width: @trunk-width-z7 + 2 * @lowzoom-halo-width; }
        [zoom >= 8] { line-width: @trunk-width-z8 + 2 * @lowzoom-halo-width; }
        [zoom >= 9] { line-width: @trunk-width-z9 + 2 * @lowzoom-halo-width; }
        [zoom >= 10] { line-width: @trunk-width-z10 + 2 * @lowzoom-halo-width; }
        [zoom >= 11] { line-width: @trunk-width-z11 + 2 * @lowzoom-halo-width; }
      }
      [feature = 'highway_primary'] {
        [zoom >= 8] { line-width: @primary-width-z8 + 2 * @lowzoom-halo-width; }
        [zoom >= 9] { line-width: @primary-width-z9 + 2 * @lowzoom-halo-width; }
        [zoom >= 10] { line-width: @primary-width-z10 + 2 * @lowzoom-halo-width; }
        [zoom >= 11] { line-width: @primary-width-z11 + 2 * @lowzoom-halo-width; }
      }
      line-color: @lowzoom-halo-color;
      line-opacity: .4;
    }
  }
  ::fill {
    [feature = 'highway_motorway'] {
      [zoom >= 6][link != 'yes'],
      [zoom >= 10] {
        line-color: @motorway-low-zoom;
        line-width: @motorway-width-z6;
        [zoom >= 7] { line-width: @motorway-width-z7; }
        [zoom >= 8] { line-width: @motorway-width-z8; }
        [zoom >= 9] { line-width: @motorway-width-z9; }
        [zoom >= 10] { line-width: @motorway-width-z10; }
        [zoom >= 11] { line-width: @motorway-width-z11; }
        [zoom >= 12] {
          line-color: @motorway-fill;
          line-width: @motorway-width-z12 - 2 * @major-casing-width-z12;
          [zoom >= 13] { line-width: @motorway-width-z13 - 2 * @major-casing-width-z13; }
          [zoom >= 15] { line-width: @motorway-width-z15 - 2 * @major-casing-width-z15; }
          [zoom >= 17] { line-width: @motorway-width-z17 - 2 * @major-casing-width-z17; }
          [zoom >= 18] { line-width: @motorway-width-z18 - 2 * @major-casing-width-z18; }
          [zoom >= 19] { line-width: @motorway-width-z19 - 2 * @major-casing-width-z19; }
          [zoom >= 20] { line-width: @motorway-width-z20 - 2 * @major-casing-width-z20; }
          [link = 'yes'] {
            line-width: @motorway-link-width-z12 - 2 * @casing-width-z12;
            [zoom >= 13] { line-width: @motorway-link-width-z13 - 2 * @casing-width-z13; }
            [zoom >= 15] { line-width: @motorway-link-width-z15 - 2 * @casing-width-z15; }
            [zoom >= 17] { line-width: @motorway-link-width-z17 - 2 * @casing-width-z17; }
            [zoom >= 18] { line-width: @motorway-link-width-z18 - 2 * @casing-width-z18; }
            [zoom >= 19] { line-width: @motorway-link-width-z19 - 2 * @casing-width-z19; }
            [zoom >= 20] { line-width: @motorway-link-width-z20 - 2 * @casing-width-z20; }
          }
          #tunnels {
            line-color: @motorway-tunnel-fill;
          }
          #bridges {
            line-width: @motorway-width-z12 - 2 * @major-bridge-casing-width-z12;
            [zoom >= 13] { line-width: @motorway-width-z13 - 2 * @major-bridge-casing-width-z13; }
            [zoom >= 15] { line-width: @motorway-width-z15 - 2 * @major-bridge-casing-width-z15; }
            [zoom >= 17] { line-width: @motorway-width-z17 - 2 * @major-bridge-casing-width-z17; }
            [zoom >= 18] { line-width: @motorway-width-z18 - 2 * @major-bridge-casing-width-z18; }
            [zoom >= 19] { line-width: @motorway-width-z19 - 2 * @major-bridge-casing-width-z19; }
            [zoom >= 20] { line-width: @motorway-width-z20 - 2 * @major-bridge-casing-width-z20; }
            [link = 'yes'] {
              line-width: @motorway-link-width-z12 - 2 * @bridge-casing-width-z12;
              [zoom >= 13] { line-width: @motorway-link-width-z13 - 2 * @bridge-casing-width-z13; }
              [zoom >= 15] { line-width: @motorway-link-width-z15 - 2 * @bridge-casing-width-z15; }
              [zoom >= 17] { line-width: @motorway-link-width-z17 - 2 * @bridge-casing-width-z17; }
              [zoom >= 18] { line-width: @motorway-link-width-z18 - 2 * @bridge-casing-width-z18; }
              [zoom >= 19] { line-width: @motorway-link-width-z19 - 2 * @bridge-casing-width-z19; }
              [zoom >= 20] { line-width: @motorway-link-width-z20 - 2 * @bridge-casing-width-z20; }
            }
          }
          line-cap: round;
          line-join: round;
        }
      }
    }

    [feature = 'highway_trunk'] {
      [zoom >= 6][link != 'yes'],
      [zoom >= 10] {
        line-width: @trunk-width-z6;
        line-color: @trunk-low-zoom;
        [zoom >= 7] { line-width: @trunk-width-z7; }
        [zoom >= 8] { line-width: @trunk-width-z8; }
        [zoom >= 9] { line-width: @trunk-width-z9; }
        [zoom >= 10] { line-width: @trunk-width-z10; }
        [zoom >= 11] { line-width: @trunk-width-z11; }
        [zoom >= 12] {
          line-color: @trunk-fill;
          line-width: @trunk-width-z12 - 2 * @major-casing-width-z12;
          [zoom >= 13] { line-width: @trunk-width-z13 - 2 * @major-casing-width-z13; }
          [zoom >= 15] { line-width: @trunk-width-z15 - 2 * @major-casing-width-z15; }
          [zoom >= 17] { line-width: @trunk-width-z17 - 2 * @major-casing-width-z17; }
          [zoom >= 18] { line-width: @trunk-width-z18 - 2 * @major-casing-width-z18; }
          [zoom >= 19] { line-width: @trunk-width-z19 - 2 * @major-casing-width-z19; }
          [link = 'yes'] {
            line-width: @trunk-link-width-z12 - 2 * @casing-width-z12;
            [zoom >= 13] { line-width: @trunk-link-width-z13 - 2 * @casing-width-z13; }
            [zoom >= 15] { line-width: @trunk-link-width-z15 - 2 * @casing-width-z15; }
            [zoom >= 17] { line-width: @trunk-link-width-z17 - 2 * @casing-width-z17; }
            [zoom >= 18] { line-width: @trunk-link-width-z18 - 2 * @casing-width-z18; }
            [zoom >= 19] { line-width: @trunk-link-width-z19 - 2 * @casing-width-z19; }
          }
          #tunnels {
            line-color: @trunk-tunnel-fill;
          }
          #bridges {
            line-width: @trunk-width-z12 - 2 * @major-bridge-casing-width-z12;
            [zoom >= 13] { line-width: @trunk-width-z13 - 2 * @major-bridge-casing-width-z13; }
            [zoom >= 15] { line-width: @trunk-width-z15 - 2 * @major-bridge-casing-width-z15; }
            [zoom >= 17] { line-width: @trunk-width-z17 - 2 * @major-bridge-casing-width-z17; }
            [zoom >= 18] { line-width: @trunk-width-z18 - 2 * @major-bridge-casing-width-z18; }
            [zoom >= 19] { line-width: @trunk-width-z19 - 2 * @major-bridge-casing-width-z19; }
            [link = 'yes'] {
              line-width: @trunk-link-width-z12 - 2 * @bridge-casing-width-z12;
              [zoom >= 13] { line-width: @trunk-link-width-z13 - 2 * @bridge-casing-width-z13; }
              [zoom >= 15] { line-width: @trunk-link-width-z15 - 2 * @bridge-casing-width-z15; }
              [zoom >= 17] { line-width: @trunk-link-width-z17 - 2 * @bridge-casing-width-z17; }
              [zoom >= 18] { line-width: @trunk-link-width-z18 - 2 * @bridge-casing-width-z18; }
              [zoom >= 19] { line-width: @trunk-link-width-z19 - 2 * @bridge-casing-width-z19; }
            }
          }
          line-cap: round;
          line-join: round;
        }
      }
    }

    [feature = 'highway_primary'] {
      [zoom >= 8][link != 'yes'],
      [zoom >= 10] {
        line-width: @primary-width-z8;
        line-color: @primary-low-zoom;
        [zoom >= 9] { line-width: @primary-width-z9; }
        [zoom >= 10] { line-width: @primary-width-z10; }
        [zoom >= 11] { line-width: @primary-width-z11; }
        [zoom >= 12] {
          line-color: @primary-fill;
          line-width: @primary-width-z12 - 2 * @major-casing-width-z12;
          [zoom >= 13] { line-width: @primary-width-z13 - 2 * @major-casing-width-z13; }
          [zoom >= 15] { line-width: @primary-width-z15 - 2 * @major-casing-width-z15; }
          [zoom >= 17] { line-width: @primary-width-z17 - 2 * @major-casing-width-z17; }
          [zoom >= 18] { line-width: @primary-width-z18 - 2 * @major-casing-width-z18; }
          [zoom >= 19] { line-width: @primary-width-z19 - 2 * @major-casing-width-z19; }
          [link = 'yes'] {
            line-width: @primary-link-width-z12 - 2 * @casing-width-z12;
            [zoom >= 13] { line-width: @primary-link-width-z13 - 2 * @casing-width-z13; }
            [zoom >= 15] { line-width: @primary-link-width-z15 - 2 * @casing-width-z15; }
            [zoom >= 17] { line-width: @primary-link-width-z17 - 2 * @casing-width-z17; }
            [zoom >= 18] { line-width: @primary-link-width-z18 - 2 * @casing-width-z18; }
            [zoom >= 19] { line-width: @primary-link-width-z19 - 2 * @casing-width-z19; }
          }
          #tunnels {
            line-color: @primary-tunnel-fill;
          }
          #bridges {
            line-width: @primary-width-z12 - 2 * @major-bridge-casing-width-z12;
            [zoom >= 13] { line-width: @primary-width-z13 - 2 * @major-bridge-casing-width-z13; }
            [zoom >= 15] { line-width: @primary-width-z15 - 2 * @major-bridge-casing-width-z15; }
            [zoom >= 17] { line-width: @primary-width-z17 - 2 * @major-bridge-casing-width-z17; }
            [zoom >= 18] { line-width: @primary-width-z18 - 2 * @major-bridge-casing-width-z18; }
            [zoom >= 19] { line-width: @primary-width-z19 - 2 * @major-bridge-casing-width-z19; }
            [link = 'yes'] {
              line-width: @primary-link-width-z12 - 2 * @bridge-casing-width-z12;
              [zoom >= 13] { line-width: @primary-link-width-z13 - 2 * @bridge-casing-width-z13; }
              [zoom >= 15] { line-width: @primary-link-width-z15 - 2 * @bridge-casing-width-z15; }
              [zoom >= 17] { line-width: @primary-link-width-z17 - 2 * @bridge-casing-width-z17; }
              [zoom >= 18] { line-width: @primary-link-width-z18 - 2 * @bridge-casing-width-z18; }
              [zoom >= 19] { line-width: @primary-link-width-z19 - 2 * @bridge-casing-width-z19; }
            }
          }
          line-cap: round;
          line-join: round;
        }
      }
    }

    [feature = 'highway_secondary'] {
      [zoom >= 12] {
        line-color: @secondary-fill;
        line-width: @secondary-width-z12 - 2 * @secondary-casing-width-z12;
        line-cap: round;
        line-join: round;
        [zoom >= 13] { line-width: @secondary-width-z13 - 2 * @secondary-casing-width-z13; }
        [zoom >= 14] { line-width: @secondary-width-z14 - 2 * @secondary-casing-width-z14; }
        [zoom >= 15] { line-width: @secondary-width-z15 - 2 * @secondary-casing-width-z15; }
        [zoom >= 16] { line-width: @secondary-width-z16 - 2 * @secondary-casing-width-z16; }
        [zoom >= 17] { line-width: @secondary-width-z17 - 2 * @secondary-casing-width-z17; }
        [zoom >= 18] { line-width: @secondary-width-z18 - 2 * @secondary-casing-width-z18; }
        [zoom >= 19] { line-width: @secondary-width-z19 - 2 * @secondary-casing-width-z19; }
        [link = 'yes'] {
          line-width: @secondary-link-width-z12 - 2 * @casing-width-z12;
          [zoom >= 13] { line-width: @secondary-link-width-z13 - 2 * @casing-width-z13; }
          [zoom >= 15] { line-width: @secondary-link-width-z15 - 2 * @casing-width-z15; }
          [zoom >= 17] { line-width: @secondary-link-width-z17 - 2 * @casing-width-z17; }
          [zoom >= 18] { line-width: @secondary-link-width-z18 - 2 * @casing-width-z18; }
          [zoom >= 19] { line-width: @secondary-link-width-z19 - 2 * @casing-width-z19; }
        }
        #tunnels {
          line-color: @secondary-tunnel-fill;
        }
        #bridges {
          line-width: @secondary-width-z12 - 2 * @bridge-casing-width-z12;
          [zoom >= 13] { line-width: @secondary-width-z13 - 2 * @major-bridge-casing-width-z13; }
          [zoom >= 14] { line-width: @secondary-width-z14 - 2 * @major-bridge-casing-width-z14; }
          [zoom >= 15] { line-width: @secondary-width-z15 - 2 * @major-bridge-casing-width-z15; }
          [zoom >= 16] { line-width: @secondary-width-z16 - 2 * @major-bridge-casing-width-z16; }
          [zoom >= 17] { line-width: @secondary-width-z17 - 2 * @major-bridge-casing-width-z17; }
          [zoom >= 18] { line-width: @secondary-width-z18 - 2 * @major-bridge-casing-width-z18; }
          [zoom >= 19] { line-width: @secondary-width-z19 - 2 * @major-bridge-casing-width-z19; }
          [link = 'yes'] {
            line-width: @secondary-link-width-z12 - 2 * @bridge-casing-width-z12;
            [zoom >= 13] { line-width: @secondary-link-width-z13 - 2 * @bridge-casing-width-z13; }
            [zoom >= 15] { line-width: @secondary-link-width-z15 - 2 * @bridge-casing-width-z15; }
            [zoom >= 17] { line-width: @secondary-link-width-z17 - 2 * @bridge-casing-width-z17; }
            [zoom >= 18] { line-width: @secondary-link-width-z18 - 2 * @bridge-casing-width-z18; }
            [zoom >= 19] { line-width: @secondary-link-width-z19 - 2 * @bridge-casing-width-z19; }
          }
        }
      }
    }

    [feature = 'highway_tertiary'] {
      [zoom >= 15] {
        line-color: @tertiary-fill;
        line-width: @tertiary-width-z15;
        [zoom >= 16] { line-width: @tertiary-width-z16 - 2 * @casing-width-z16; }
        [zoom >= 17] { line-width: @tertiary-width-z17 - 2 * @casing-width-z17; }
        [zoom >= 18] { line-width: @tertiary-width-z18 - 2 * @casing-width-z18; }
        [zoom >= 19] { line-width: @tertiary-width-z19 - 2 * @casing-width-z19; }
        [link = 'yes'] {
          line-width: @tertiary-link-width-z15 - 2 * @casing-width-z15;
          [zoom >= 17] { line-width: @tertiary-link-width-z17 - 2 * @casing-width-z17; }
          [zoom >= 18] { line-width: @tertiary-link-width-z18 - 2 * @casing-width-z18; }
          [zoom >= 19] { line-width: @tertiary-link-width-z19 - 2 * @casing-width-z19; }
        }
        #tunnels {
          line-color: @tertiary-tunnel-fill;
        }
        #bridges {
          line-width: @tertiary-width-z15 - 2 * @bridge-casing-width-z15;
          [zoom >= 16] { line-width: @tertiary-width-z16 - 2 * @bridge-casing-width-z16; }
          [zoom >= 17] { line-width: @tertiary-width-z17 - 2 * @bridge-casing-width-z17; }
          [zoom >= 18] { line-width: @tertiary-width-z18 - 2 * @bridge-casing-width-z18; }
          [zoom >= 19] { line-width: @tertiary-width-z19 - 2 * @bridge-casing-width-z19; }
          [link = 'yes'] {
            line-width: @tertiary-link-width-z15 - 2 * @bridge-casing-width-z15;
            [zoom >= 17] { line-width: @tertiary-link-width-z17 - 2 * @bridge-casing-width-z17; }
            [zoom >= 18] { line-width: @tertiary-link-width-z18 - 2 * @bridge-casing-width-z18; }
            [zoom >= 19] { line-width: @tertiary-link-width-z19 - 2 * @bridge-casing-width-z19; }
          }
        }
        line-cap: round;
        line-join: round;
      }
     }

    [feature = 'highway_living_street'] {
      [zoom >= 13] {
        line-width: @living-street-width-z15 - 2 * @casing-width-z15;
        [zoom >= 16] { line-width: @living-street-width-z16 - 2 * @casing-width-z16; }
        [zoom >= 17] { line-width: @living-street-width-z17 - 2 * @casing-width-z17; }
        [zoom >= 18] { line-width: @living-street-width-z18 - 2 * @casing-width-z18; }
        [zoom >= 19] { line-width: @living-street-width-z19 - 2 * @casing-width-z19; }
        #roads-fill, #bridges {
          line-color: @living-street-fill;
        }
        #tunnels {
          line-color: @living-street-tunnel-fill;
        }
        #bridges {
          line-width: @living-street-width-z15 - 2 * @casing-width-z15;
          [zoom >= 16] { line-width: @living-street-width-z16 - 2 * @bridge-casing-width-z16; }
          [zoom >= 17] { line-width: @living-street-width-z17 - 2 * @bridge-casing-width-z17; }
          [zoom >= 18] { line-width: @living-street-width-z18 - 2 * @bridge-casing-width-z18; }
          [zoom >= 19] { line-width: @living-street-width-z19 - 2 * @bridge-casing-width-z19; }
        }
        line-join: round;
        line-cap: round;
      }
    }

    [feature = 'highway_road'] {
      [zoom >= 10] {
        line-width: 1;
        line-color: @residential-fill;
        line-join: round;
        line-cap: round;
      }
      [zoom >= 15] {
        line-width: @road-width-z15 - 2 * @casing-width-z15;
        [zoom >= 16] { line-width: @road-width-z16 - 2 * @casing-width-z16; }
        [zoom >= 17] { line-width: @road-width-z17 - 2 * @casing-width-z17; }
        [zoom >= 18] { line-width: @road-width-z18 - 2 * @casing-width-z18; }
        [zoom >= 19] { line-width: @road-width-z19 - 2 * @casing-width-z19; }
        #roads-fill {
          line-color: @road-fill;
        }
        #bridges {
          line-width: @road-width-z15 - 2 * @bridge-casing-width-z15;
          [zoom >= 16] { line-width: @road-width-z16 - 2 * @bridge-casing-width-z16; }
          [zoom >= 17] { line-width: @road-width-z17 - 2 * @bridge-casing-width-z17; }
          [zoom >= 18] { line-width: @road-width-z18 - 2 * @bridge-casing-width-z18; }
          [zoom >= 19] { line-width: @road-width-z19 - 2 * @bridge-casing-width-z19; }
          line-color: @road-fill;
        }
        #tunnels {
          line-color: @road-fill;
        }
      }
    }

    [feature = 'highway_service'] {
      [zoom >= 14][service = 'INT-normal'],
      [zoom >= 16][service = 'INT-minor'] {
        line-color: @service-fill;
        [service = 'INT-normal'] {
          line-width: @service-width-z16 - 2 * @casing-width-z16;
          [zoom >= 17] { line-width: @service-width-z17 - 2 * @casing-width-z17; }
          [zoom >= 18] { line-width: @service-width-z18 - 2 * @casing-width-z18; }
          [zoom >= 19] { line-width: @service-width-z19 - 2 * @casing-width-z19; }
          [zoom >= 20] { line-width: @service-width-z20 - 2 * @casing-width-z20; }
        }
        [service = 'INT-minor'] {
          line-width: @minor-service-width-z16 - 2 * @casing-width-z16;
          [zoom >= 17] { line-width: @minor-service-width-z17 - 2 * @casing-width-z17; }
          [zoom >= 18] { line-width: @minor-service-width-z18 - 2 * @casing-width-z18; }
          [zoom >= 19] { line-width: @minor-service-width-z19 - 2 * @casing-width-z19; }
          [zoom >= 20] { line-width: @minor-service-width-z20 - 2 * @casing-width-z20; }
        }
        line-join: round;
        line-cap: round;
        #tunnels {
          line-color: darken(white, 5%);
        }
        #bridges {
          [service = 'INT-normal'] {
            line-width: @service-width-z16 - 2 * @bridge-casing-width-z16;
            [zoom >= 17] { line-width: @service-width-z17 - 2 * @bridge-casing-width-z17; }
            [zoom >= 18] { line-width: @service-width-z18 - 2 * @bridge-casing-width-z18; }
            [zoom >= 19] { line-width: @service-width-z19 - 2 * @bridge-casing-width-z19; }
            [zoom >= 20] { line-width: @service-width-z20 - 2 * @bridge-casing-width-z20; }
          }
          [service = 'INT-minor'] {
            line-width: @minor-service-width-z16 - 2 * @bridge-casing-width-z16;
            [zoom >= 17] { line-width: @minor-service-width-z17 - 2 * @bridge-casing-width-z17; }
            [zoom >= 18] { line-width: @minor-service-width-z18 - 2 * @bridge-casing-width-z18; }
            [zoom >= 19] { line-width: @minor-service-width-z19 - 2 * @bridge-casing-width-z19; }
            [zoom >= 20] { line-width: @minor-service-width-z20 - 2 * @bridge-casing-width-z20; }
          }
        }
      }
    }

    [feature = 'highway_raceway'] {
      [zoom >= 12] {
        line-color: @raceway-fill;
        line-width: 1.2;
        line-join: round;
        line-cap: round;
      }
      [zoom >= 13] { line-width: 2; }
      [zoom >= 14] { line-width: 3; }
      [zoom >= 15] { line-width: 6; }
      [zoom >= 18] { line-width: 8; }
      [zoom >= 19] { line-width: 12; }
      [zoom >= 20] { line-width: 24; }
    }

    [feature = 'highway_platform'] {
      [zoom >= 16] {
        line-join: round;
        line-width: 6;
        line-color: grey;
        line-cap: round;
        b/line-width: 4;
        b/line-color: #bbbbbb;
        b/line-cap: round;
        b/line-join: round;
      }
    }

    [feature = 'railway_rail'][zoom >= 8],
    [feature = 'railway_INT-spur-siding-yard'][zoom >= 13] {
      [zoom < 13] {
        line-color: #787878;
        line-width: 0.5;
        [zoom >= 8] { line-width: 0.8; }
        [zoom >= 12] { line-width: 0.9; }
        line-join: round;
        .roads_low_zoom[int_tunnel = 'yes'], #tunnels {
          line-dasharray: 5,2;
        }
      }
      [zoom >= 12] {
        #roads-fill, #bridges {
          dark/line-join: round;
          light/line-color: white;
          light/line-join: round;
          [feature = 'railway_rail'] {
            dark/line-color: #707070;
            dark/line-width: 2;
            light/line-width: 0.75;
            light/line-dasharray: 8,8;
            [zoom >= 13] {
              dark/line-width: 3;
              light/line-width: 1;
            }
            [zoom >= 15] {
              light/line-dasharray: 0,8,8,1;
            }
            [zoom >= 18] {
              dark/line-width: 4;
              light/line-width: 2;
            }
          }
          [feature = 'railway_INT-spur-siding-yard'] {
            dark/line-width: 2;
            dark/line-color: #aaa;
            light/line-width: 0.8;
            light/line-dasharray: 0,8,8,1;
            [zoom >= 18] {
              dark/line-width: 3;
              light/line-width: 1;
            }
          }
        }
        #tunnels {
          line-color: #787878;
          line-width: 2.8;
          line-dasharray: 6,4;
          line-clip: false;
          [feature = 'railway_INT-spur-siding-yard'] {
            line-color: #aaa;
            line-width: 1.9;
            line-dasharray: 3,3;
            [zoom >= 18] {
            line-width: 2.7;
            }
          }
          [feature = 'railway_rail'][zoom >= 18] {
            line-dasharray: 8,6;
            line-width: 3.8;
          }
        }
      }
    }

    [feature = 'railway_light_rail'],
    [feature = 'railway_funicular'],
    [feature = 'railway_narrow_gauge'] {
      [zoom >= 8] {
        line-color: #ccc;
        [zoom >= 10] { line-color: #aaa; }
        [zoom >= 13] { line-color: #666; }
        line-width: 1;
        [zoom >= 13] { line-width: 2; }
        #tunnels {
          line-dasharray: 5,3;
        }
      }
    }

    [feature = 'railway_miniature'] {
      [zoom >= 15] {
        line/line-width: 1.2;
        line/line-color: #999;
        dashes/line-width: 3;
        dashes/line-color: #999;
        dashes/line-dasharray: 1,10;
      }
    }

    [feature = 'railway_tram'],
    [feature = 'railway_tram-service'][zoom >= 15] {
      [zoom >= 12] {
        line-color: #6E6E6E;
        line-width: 0.75;
        [zoom >= 14] {
          line-width: 1;
        }
        [zoom >= 15] {
          line-width: 1.5;
          [feature = 'railway_tram-service'] {
            line-width: 0.5;
          }
        }
        [zoom >= 17] {
          line-width: 2;
          [feature = 'railway_tram-service'] {
            line-width: 1;
          }
        }
        [zoom >= 18] {
          [feature = 'railway_tram-service'] {
            line-width: 1.5;
          }
        }
        [zoom >= 19] {
          [feature = 'railway_tram-service'] {
            line-width: 2;
          }
        }
        #tunnels {
          line-dasharray: 5,3;
        }
      }
    }

    [feature = 'railway_subway'] {
      [zoom >= 12] {
        line-width: 2;
        line-color: #999;
        #tunnels {
          line-dasharray: 5,3;
        }
      }
      #bridges {
        [zoom >= 14] {
          line-width: 2;
          line-color: #999;
        }
      }
    }

    [feature = 'railway_preserved'] {
      [zoom >= 12] {
        dark/line-width: 1.5;
        dark/line-color: #aaa;
        dark/line-join: round;
        [zoom >= 13] {
          dark/line-width: 3;
          dark/line-color: #999999;
          light/line-width: 1;
          light/line-color: white;
          light/line-dasharray: 0,1,8,1;
          light/line-join: round;
        }
      }
    }

    [feature = 'railway_INT-preserved-ssy'] {
      [zoom >= 12] {
        dark/line-width: 1;
        dark/line-color: #aaa;
        dark/line-join: round;
        [zoom >= 13] {
          dark/line-width: 2;
          dark/line-color: #999999;
          light/line-width: 0.8;
          light/line-color: white;
          light/line-dasharray: 0,1,8,1;
          light/line-join: round;
        }
      }
    }

    [feature = 'railway_monorail'] {
      [zoom >= 14] {
        background/line-width: 4;
        background/line-color: #fff;
        background/line-opacity: 0.4;
        background/line-cap: round;
        background/line-join: round;
        line/line-width: 3;
        line/line-color: #777;
        line/line-dasharray: 2,3;
        line/line-cap: round;
        line/line-join: round;
      }
    }

    [feature = 'railway_construction'] {
      [zoom >= 13] {
        line-color: grey;
        line-width: 2;
        line-dasharray: 2,4;
        line-join: round;
        [zoom >= 14] {
          line-dasharray: 2,3;
        }
        [zoom >= 15] {
          line-width: 3;
          line-dasharray: 3,3;
        }
      }
    }

    [feature = 'railway_disused'] {
      [zoom >= 15] {
        line-color: #aaa;
        line-width: 2;
        line-dasharray: 2,4;
        line-join: round;
      }
    }

    [feature = 'railway_platform'] {
      [zoom >= 16] {
        line-join: round;
        line-width: 6;
        line-color: grey;
        line-cap: round;
        b/line-width: 4;
        b/line-color: #bbbbbb;
        b/line-cap: round;
        b/line-join: round;
      }
    }

    [feature = 'railway_turntable'] {
      [zoom >= 16] {
        line-width: 1.5;
        line-color: #999;
      }
    }
  }
}
/*
This whole section all the way up to the " Data on z<10 comes from" had 
been commented at one point for some reason. This will stay as a bookmark 
in case it is needed later.
*/
#turning-circle-casing {
  [int_tc_type = 'primary'][zoom >= 15] {
    marker-fill: @primary-casing;
    marker-width: @primary-width-z15 * 1.6 + 2 * @casing-width-z15;
    marker-height: @primary-width-z15 * 1.6 + 2 * @casing-width-z15;
    [zoom >= 17] {
      marker-width: @primary-width-z17 * 1.6 + 2 * @casing-width-z17;
      marker-height: @primary-width-z17 * 1.6 + 2 * @casing-width-z17;
    }
    [zoom >= 18] {
      marker-width: @primary-width-z18 * 1.6 + 2 * @casing-width-z18;
      marker-height: @primary-width-z18 * 1.6 + 2 * @casing-width-z18;
    }
    [zoom >= 19] {
      marker-width: @primary-width-z19 * 1.6 + 2 * @casing-width-z19;
      marker-height: @primary-width-z19 * 1.6 + 2 * @casing-width-z19;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'secondary'][zoom >= 15] {
    marker-fill: @secondary-casing;
    marker-width: @secondary-width-z15 * 1.6 + 2 * @casing-width-z15;
    marker-height: @secondary-width-z15 * 1.6 + 2 * @casing-width-z15;
    [zoom >= 16] {
      marker-width: @secondary-width-z16 * 1.6 + 2 * @casing-width-z16;
      marker-height: @secondary-width-z16 * 1.6 + 2 * @casing-width-z16;
    }
    [zoom >= 17] {
      marker-width: @secondary-width-z17 * 1.6 + 2 * @casing-width-z17;
      marker-height: @secondary-width-z17 * 1.6 + 2 * @casing-width-z17;
    }
    [zoom >= 18] {
      marker-width: @secondary-width-z18 * 1.6 + 2 * @casing-width-z18;
      marker-height: @secondary-width-z18 * 1.6 + 2 * @casing-width-z18;
    }
    [zoom >= 19] {
      marker-width: @secondary-width-z19 * 1.6 + 2 * @casing-width-z19;
      marker-height: @secondary-width-z19 * 1.6 + 2 * @casing-width-z19;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'tertiary'][zoom >= 15] {
    marker-fill: @tertiary-casing;
    marker-width: @tertiary-width-z15 * 1.6 + 2 * @casing-width-z15;
    marker-height: @tertiary-width-z15 * 1.6 + 2 * @casing-width-z15;
    [zoom >= 16] {
      marker-width: @tertiary-width-z16 * 1.6 + 2 * @casing-width-z16;
      marker-height: @tertiary-width-z16 * 1.6 + 2 * @casing-width-z16;
    }
    [zoom >= 17] {
      marker-width: @tertiary-width-z17 * 1.6 + 2 * @casing-width-z17;
      marker-height: @tertiary-width-z17 * 1.6 + 2 * @casing-width-z17;
    }
    [zoom >= 18] {
      marker-width: @tertiary-width-z18 * 1.6 + 2 * @casing-width-z18;
      marker-height: @tertiary-width-z18 * 1.6 + 2 * @casing-width-z18;
    }
    [zoom >= 19] {
      marker-width: @tertiary-width-z19 * 1.6 + 2 * @casing-width-z19;
      marker-height: @tertiary-width-z19 * 1.6 + 2 * @casing-width-z19;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'residential'][zoom >= 15],
  [int_tc_type = 'unclassified'][zoom >= 15] {
    marker-fill: @residential-casing;
    marker-width: @residential-width-z15 * 1.6 + 2 * @casing-width-z15;
    marker-height: @residential-width-z15 * 1.6 + 2 * @casing-width-z15;
    [zoom >= 16] {
      marker-width: @residential-width-z16 * 1.6 + 2 * @casing-width-z16;
      marker-height: @residential-width-z16 * 1.6 + 2 * @casing-width-z16;
    }
    [zoom >= 17] {
      marker-width: @residential-width-z17 * 1.6 + 2 * @casing-width-z17;
      marker-height: @residential-width-z17 * 1.6 + 2 * @casing-width-z17;
    }
    [zoom >= 18] {
      marker-width: @residential-width-z18 * 1.6 + 2 * @casing-width-z18;
      marker-height: @residential-width-z18 * 1.6 + 2 * @casing-width-z18;
    }
    [zoom >= 19] {
      marker-width: @residential-width-z19 * 1.6 + 2 * @casing-width-z19;
      marker-height: @residential-width-z19 * 1.6 + 2 * @casing-width-z19;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'living_street'][zoom >= 15] {
    marker-fill: @living-street-casing;
    marker-width: @living-street-width-z15 * 1.6 + 2 * @casing-width-z15;
    marker-height: @living-street-width-z15 * 1.6 + 2 * @casing-width-z15;
    [zoom >= 16] {
      marker-width: @living-street-width-z16 * 1.6 + 2 * @casing-width-z16;
      marker-height: @living-street-width-z16 * 1.6 + 2 * @casing-width-z16;
    }
    [zoom >= 17] {
      marker-width: @living-street-width-z17 * 1.6 + 2 * @casing-width-z17;
      marker-height: @living-street-width-z17 * 1.6 + 2 * @casing-width-z17;
    }
    [zoom >= 18] {
      marker-width: @living-street-width-z18 * 1.6 + 2 * @casing-width-z18;
      marker-height: @living-street-width-z18 * 1.6 + 2 * @casing-width-z18;
    }
    [zoom >= 19] {
      marker-width: @living-street-width-z19 * 1.6 + 2 * @casing-width-z19;
      marker-height: @living-street-width-z19 * 1.6 + 2 * @casing-width-z19;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'service'][int_tc_service = 'INT-normal'][zoom >= 16] {
    marker-fill: @service-casing;
    marker-width: @service-width-z16 * 1.6 + 2 * @casing-width-z16;
    marker-height: @service-width-z16 * 1.6 + 2 * @casing-width-z16;
    [zoom >= 17] {
      marker-width: @service-width-z17 * 1.6 + 2 * @casing-width-z17;
      marker-height: @service-width-z17 * 1.6 + 2 * @casing-width-z17;
    }
    [zoom >= 18] {
      marker-width: @service-width-z18 * 1.6 + 2 * @casing-width-z18;
      marker-height: @service-width-z18 * 1.6 + 2 * @casing-width-z18;
    }
    [zoom >= 19] {
      marker-width: @service-width-z19 * 1.6 + 2 * @casing-width-z19;
      marker-height: @service-width-z19 * 1.6 + 2 * @casing-width-z19;
    }
    [zoom >= 20] {
      marker-width: @service-width-z20 * 1.6 + 2 * @casing-width-z20;
      marker-height: @service-width-z20 * 1.6 + 2 * @casing-width-z20;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'service'][int_tc_service = 'INT-minor'][zoom >= 18] {
    marker-fill: @service-casing;
    marker-width: @minor-service-width-z18 * 1.6 + 2 * @casing-width-z18;
    marker-height: @minor-service-width-z18 * 1.6 + 2 * @casing-width-z18;
    [zoom >= 19] {
      marker-width: @minor-service-width-z19 * 1.6 + 2 * @casing-width-z19;
      marker-height: @minor-service-width-z19 * 1.6 + 2 * @casing-width-z19;
    }
    [zoom >= 20] {
      marker-width: @minor-service-width-z20 * 1.6 + 2 * @casing-width-z20;
      marker-height: @minor-service-width-z20 * 1.6 + 2 * @casing-width-z20;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }
}

#turning-circle-fill {
  [int_tc_type = 'primary'][zoom >= 15] {
    marker-fill: @primary-fill;
    marker-width: @primary-width-z15 * 1.6;
    marker-height: @primary-width-z15 * 1.6;
    [zoom >= 17] {
      marker-width: @primary-width-z17 * 1.6;
      marker-height: @primary-width-z17 * 1.6;
    }
    [zoom >= 18] {
      marker-width: @primary-width-z18 * 1.6;
      marker-height: @primary-width-z18 * 1.6;
    }
    [zoom >= 19] {
      marker-width: @primary-width-z19 * 1.6;
      marker-height: @primary-width-z19 * 1.6;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'secondary'][zoom >= 15] {
    marker-fill: @secondary-fill;
    marker-width: @secondary-width-z15 * 1.6;
    marker-height: @secondary-width-z15 * 1.6;
    [zoom >= 16] {
      marker-width: @secondary-width-z16 * 1.6;
      marker-height: @secondary-width-z16 * 1.6;
    }
    [zoom >= 17] {
      marker-width: @secondary-width-z17 * 1.6;
      marker-height: @secondary-width-z17 * 1.6;
    }
    [zoom >= 18] {
      marker-width: @secondary-width-z18 * 1.6;
      marker-height: @secondary-width-z18 * 1.6;
    }
    [zoom >= 19] {
      marker-width: @secondary-width-z19 * 1.6;
      marker-height: @secondary-width-z19 * 1.6;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'tertiary'][zoom >= 15] {
    marker-fill: @tertiary-fill;
    marker-width: @tertiary-width-z15 * 1.6;
    marker-height: @tertiary-width-z15 * 1.6;
    [zoom >= 16] {
      marker-width: @tertiary-width-z16 * 1.6;
      marker-height: @tertiary-width-z16 * 1.6;
    }
    [zoom >= 17] {
      marker-width: @tertiary-width-z17 * 1.6;
      marker-height: @tertiary-width-z17 * 1.6;
    }
    [zoom >= 18] {
      marker-width: @tertiary-width-z18 * 1.6;
      marker-height: @tertiary-width-z18 * 1.6;
    }
    [zoom >= 19] {
      marker-width: @tertiary-width-z19 * 1.6;
      marker-height: @tertiary-width-z19 * 1.6;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'residential'],
  [int_tc_type = 'unclassified'] {
    [zoom >= 15] {
      marker-fill: @residential-fill;
      marker-width: @residential-width-z15 * 1.6;
      marker-height: @residential-width-z15 * 1.6;
      [zoom >= 16] {
        marker-width: @residential-width-z16 * 1.6;
        marker-height: @residential-width-z16 * 1.6;
      }
      [zoom >= 17] {
        marker-width: @residential-width-z17 * 1.6;
        marker-height: @residential-width-z17 * 1.6;
      }
      [zoom >= 18] {
        marker-width: @residential-width-z18 * 1.6;
        marker-height: @residential-width-z18 * 1.6;
      }
      [zoom >= 19] {
        marker-width: @residential-width-z19 * 1.6;
        marker-height: @residential-width-z19 * 1.6;
      }
      marker-allow-overlap: true;
      marker-ignore-placement: true;
      marker-line-width: 0;
    }
  }

  [int_tc_type = 'living_street'][zoom >= 15] {
    marker-fill: @living-street-fill;
    marker-width: @living-street-width-z15 * 1.6;
    marker-height: @living-street-width-z15 * 1.6;
    [zoom >= 16] {
      marker-width: @living-street-width-z16 * 1.6;
      marker-height: @living-street-width-z16 * 1.6;
    }
    [zoom >= 17] {
      marker-width: @living-street-width-z17 * 1.6;
      marker-height: @living-street-width-z17 * 1.6;
    }
    [zoom >= 18] {
      marker-width: @living-street-width-z18 * 1.6;
      marker-height: @living-street-width-z18 * 1.6;
    }
    [zoom >= 19] {
      marker-width: @living-street-width-z19 * 1.6;
      marker-height: @living-street-width-z19 * 1.6;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'service'][int_tc_service = 'INT-normal'][zoom >= 16] {
    marker-fill: @service-fill;
    marker-width: @service-width-z16 * 1.6;
    marker-height: @service-width-z16 * 1.6;
    [zoom >= 17] {
      marker-width: @service-width-z17 * 1.6;
      marker-height: @service-width-z17 * 1.6;
    }
    [zoom >= 18] {
      marker-width: @service-width-z18 * 1.6;
      marker-height: @service-width-z18 * 1.6;
    }
    [zoom >= 19] {
      marker-width: @service-width-z19 * 1.6;
      marker-height: @service-width-z19 * 1.6;
    }
    [zoom >= 20] {
      marker-width: @service-width-z20 * 1.6;
      marker-height: @service-width-z20 * 1.6;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'service'][int_tc_service = 'INT-minor'][zoom >= 18] {
    marker-fill: @service-fill;
    marker-width: @minor-service-width-z18 * 1.6;
    marker-height: @minor-service-width-z18 * 1.6;
    [zoom >= 19] {
      marker-width: @minor-service-width-z19 * 1.6;
      marker-height: @minor-service-width-z19 * 1.6;
    }
    [zoom >= 20] {
      marker-width: @minor-service-width-z20 * 1.6;
      marker-height: @minor-service-width-z20 * 1.6;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [type = 'mini_roundabout']::circle {
    marker-width: @mini-roundabout-width;
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;

    [int_tc_type = 'primary'] { marker-fill: @primary-casing; }
    [int_tc_type = 'secondary'] { marker-fill: @secondary-casing; }
    [int_tc_type = 'tertiary'] { marker-fill: @primary-casing; }
    [int_tc_type = 'residential'] { marker-fill: @residential-casing; }
    [int_tc_type = 'living_street'] { marker-fill: @living-street-casing; }
    [int_tc_type = 'service'] { marker-fill: @service-casing; }
  }
}

#highway-area-casing {
  [feature = 'highway_service'] {
    [zoom >= 14] {
      line-color: #999;
      line-width: 1;
    }
  }

  [feature = 'highway_platform'],
  [feature = 'railway_platform'] {
    [zoom >= 16] {
      line-color: grey;
      line-width: 2;
      line-cap: round;
      line-join: round;
    }
  }
}

#highway-area-fill {
  [feature = 'highway_living_street'][zoom >= 14] {
    polygon-fill: @living-street-fill;
  }

  [feature = 'highway_service'] {
    [zoom >= 14] {
      polygon-fill: #fff;
    }
  }

  [feature = 'highway_platform'],
  [feature = 'railway_platform'] {
    [zoom >= 16] {
      polygon-fill: #bbbbbb;
      polygon-gamma: 0.65;
    }
  }
}

#tunnels::fill,
#roads-fill::fill,
#bridges::fill {
  [access = 'destination'] {
    [feature = 'highway_secondary'],
    [feature = 'highway_tertiary'],
    [feature = 'highway_unclassified'],
    [feature = 'highway_residential'],
    [feature = 'highway_living_street'] {
      [zoom >= 15] {
        access/line-color: @access-marking;
        [feature = 'highway_living_street'] {
          access/line-color: @access-marking-living-street;
        }
        access/line-join: round;
        access/line-cap: round;
        access/line-width: 3;
        access/line-dasharray: 0.1,9;
        [zoom >= 17] {
          access/line-width: 6;
          access/line-dasharray: 0.1,12;
        }
      }
    }
    [feature = 'highway_road'],
    [feature = 'highway_service'][service = 'INT-normal'] {
      [zoom >= 15] {
        access/line-color: @access-marking;
        access/line-join: round;
        access/line-cap: round;
        access/line-width: 2;
        access/line-dasharray: 0.1,4;
        [zoom >= 17] {
          access/line-width: 4;
          access/line-dasharray: 0.1,9;
        }
      }
    }
    [feature = 'highway_service'][service = 'INT-minor'] {
      [zoom >= 16] {
        access/line-color: @access-marking;
        access/line-join: round;
        access/line-cap: round;
        access/line-width: 1;
        access/line-dasharray: 0.1,4;
        [zoom >= 17] {
          access/line-width: 2;
        }
      }
    }
  }
  [access = 'no'] {
    [feature = 'highway_motorway'],
    [feature = 'highway_trunk'],
    [feature = 'highway_primary'],
    [feature = 'highway_secondary'],
    [feature = 'highway_tertiary'],
    [feature = 'highway_unclassified'],
    [feature = 'highway_residential'],
    [feature = 'highway_living_street'] {
      [zoom >= 15] {
        access/line-color: @access-marking;
        [feature = 'highway_living_street'] {
          access/line-color: @access-marking-living-street;
        }
        access/line-join: round;
        access/line-cap: round;
        access/line-width: 2;
        access/line-dasharray: 6,6;
        [zoom >= 17] {
          access/line-width: 6;
          access/line-dasharray: 10,12;
        }
      }
    }
    [feature = 'highway_road'],
    [feature = 'highway_service'][service = 'INT-normal'] {
      [zoom >= 15] {
        access/line-color: @access-marking;
        access/line-join: round;
        access/line-cap: round;
        access/line-width: 2;
        access/line-dasharray: 6,8;
        [zoom >= 17] {
          access/line-width: 3;
          access/line-dasharray: 8,10;
        }
      }
    }
    [feature = 'highway_service'][service = 'INT-minor'][zoom >= 16] {
      access/line-color: @access-marking;
      access/line-join: round;
      access/line-cap: round;
      access/line-width: 1;
      access/line-dasharray: 6,8;
      [zoom >= 17] {
        access/line-width: 2;
      }
    }
  }
}

#roads-text-ref-minor {
  [highway = 'unclassified'],
  [highway = 'residential'] {
    [zoom >= 15] {
      text-name: "[refs]";
      text-size: 8;

      [zoom >= 16] {
        text-size: 9;
      }
      [zoom >= 17] {
        text-size: 11;
      }

      text-fill: #000;
      text-face-name: @oblique-fonts;
      text-placement: line;
      text-repeat-distance: @major-highway-text-repeat-distance;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
      text-spacing: 760;
      text-clip: false;
    }
  }
}

#roads-text-name {
  [highway = 'motorway'],
  [highway = 'trunk'],
  [highway = 'primary'],
  [highway = 'construction'][construction = 'motorway'],
  [highway = 'construction'][construction = 'trunk'],
  [highway = 'construction'][construction = 'primary'] {
    [zoom >= 13] {
      text-name: "[name]";
      text-size: 8;
      text-fill: black;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-face-name: @book-fonts;
      text-repeat-distance: @major-highway-text-repeat-distance;
      [tunnel = 'no'] {
        text-halo-radius: @standard-halo-radius;
        [highway = 'motorway'] { text-halo-fill: @motorway-fill; }
        [highway = 'trunk'] { text-halo-fill: @trunk-fill; }
        [highway = 'primary'] { text-halo-fill: @primary-fill; }
      }
    }
    [zoom >= 14] {
      text-size: 9;
    }
    [zoom >= 15] {
      text-size: 10;
    }
    [zoom >= 17] {
      text-size: 11;
    }
    [zoom >= 19] {
      text-size: 12;
    }
  }
  [highway = 'secondary'],
  [highway = 'construction'][construction = 'secondary'] {
    [zoom >= 13] {
      text-name: "[name]";
      text-size: 8;
      text-fill: black;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-face-name: @book-fonts;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @secondary-fill;
      text-repeat-distance: @major-highway-text-repeat-distance;
    }
    [zoom >= 14] {
      text-size: 9;
    }
    [zoom >= 15] {
      text-size: 10;
    }
    [zoom >= 17] {
      text-size: 11;
    }
    [zoom >= 19] {
      text-size: 12;
    }
  }
  [highway = 'tertiary'],
  [highway = 'construction'][construction = 'tertiary'] {
    [zoom >= 14] {
      text-name: "[name]";
      text-size: 9;
      text-fill: black;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-face-name: @book-fonts;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @tertiary-fill;
      text-repeat-distance: @major-highway-text-repeat-distance;
    }
    [zoom >= 17] {
      text-size: 11;
    }
    [zoom >= 19] {
      text-size: 12;
    }
  }
  [highway = 'construction'][construction = null][zoom >= 16] {
    text-name: "[name]";
    text-size: 9;
    text-fill: black;
    text-spacing: 300;
    text-clip: false;
    text-placement: line;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
    text-face-name: @book-fonts;
    text-repeat-distance: @major-highway-text-repeat-distance;

    [zoom >= 17] {
      text-size: 11;
      text-spacing: 400;
    }
    [zoom >= 19] {
      text-size: 12;
      text-spacing: 400;
    }
  }
  [highway = 'residential'],
  [highway = 'unclassified'],
  [highway = 'road'],
  [highway = 'construction'][construction = null],
  [highway = 'construction'][construction = 'residential'],
  [highway = 'construction'][construction = 'unclassified'],
  [highway = 'construction'][construction = 'road'] {
    [zoom >= 15] {
      text-name: "[name]";
      text-size: 8;
      text-fill: black;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @residential-fill;
      text-face-name: @book-fonts;
      text-repeat-distance: @minor-highway-text-repeat-distance;
      [highway = 'unclassified'] { text-repeat-distance: @major-highway-text-repeat-distance;}
    }
    [zoom >= 16] {
      text-size: 9;
    }
    [zoom >= 17] {
      text-size: 11;
      text-spacing: 400;
    }
    [zoom >= 19] {
      text-size: 12;
      text-spacing: 400;
    }
  }

  [highway = 'raceway'],
  [highway = 'service'],
  [highway = 'construction'][construction = 'raceway'],
  [highway = 'construction'][construction = 'service'][zoom >= 17] {
    [zoom >= 16] {
      text-name: "[name]";
      text-size: 9;
      text-fill: black;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-halo-radius: @standard-halo-radius;
      [highway = 'raceway'] { text-halo-fill: @raceway-fill; }
      [highway = 'service'] { text-halo-fill: @service-fill; }
      text-face-name: @book-fonts;
      text-repeat-distance: @major-highway-text-repeat-distance;
    }
    [zoom >= 17] {
      text-size: 11;
    }
  }
  
  [highway = 'living_street'],
  [highway = 'pedestrian'],
  [highway = 'construction'][construction = 'living_street'][zoom >= 16],
  [highway = 'construction'][construction = 'pedestrian'][zoom >= 16] {
    [zoom >= 15] {
      text-name: "[name]";
      text-size: 8;
      text-fill: black;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-halo-radius: @standard-halo-radius;
      [highway = 'living_street'] {
        text-halo-fill: @living-street-fill;
        text-repeat-distance: @major-highway-text-repeat-distance;
      }
      [highway = 'pedestrian'] { text-halo-fill: @pedestrian-fill; }
      text-face-name: @book-fonts;
      text-repeat-distance: @minor-highway-text-repeat-distance;
    }
    [zoom >= 16] {
      text-size: 9;
    }
    [zoom >= 17] {
      text-size: 11;
    }
    [zoom >= 19] {
      text-size: 12;
    }
  }
}

#roads-area-text-name {
  [way_pixels > 3000],
  [zoom >= 17] {
    text-name: "[name]";
    text-size: 11;
    text-face-name: @book-fonts;
    text-wrap-width: 30; // 2.7 em
    text-line-spacing: -1.7; // -0.15 em
  }
}

#roads-text-name::directions,
#paths-text-name::directions {
  [zoom >= 16] {
    // intentionally omitting highway_platform, highway_construction
    [highway = 'motorway'],
    [highway = 'motorway_link'],
    [highway = 'trunk'],
    [highway = 'trunk_link'],
    [highway = 'primary'],
    [highway = 'primary_link'],
    [highway = 'secondary'],
    [highway = 'secondary_link'],
    [highway = 'tertiary'],
    [highway = 'tertiary_link'],
    [highway = 'residential'],
    [highway = 'unclassified'],
    [highway = 'living_street'],
    [highway = 'road'],
    [highway = 'service'],
    [highway = 'pedestrian'],
    [highway = 'raceway'] {
      [oneway = 'yes'],
      [oneway = '-1'] {
        marker-placement: line;
        marker-spacing: 180;
        marker-max-error: 0.5;
        marker-file: url('symbols/oneway.svg');
        [oneway = '-1'] {
          marker-file: url('symbols/oneway-reverse.svg');
        }

        [highway = 'motorway'],
        [highway = 'motorway_link'] {
          marker-fill: @motorway-oneway-arrow-color;
        }
        [highway = 'trunk'],
        [highway = 'trunk_link'] {
          marker-fill: @trunk-oneway-arrow-color;
        }
        [highway = 'primary'],
        [highway = 'primary_link'] {
          marker-fill: @primary-oneway-arrow-color;
        }
        [highway = 'secondary'],
        [highway = 'secondary_link'] {
          marker-fill: @secondary-oneway-arrow-color;
        }
        [highway = 'tertiary'],
        [highway = 'tertiary_link'] {
          marker-fill: @tertiary-oneway-arrow-color;
        }
        [highway = 'residential'],
        [highway = 'unclassified'],
        [highway = 'road'],
        [highway = 'service'] {
          marker-fill: @residential-oneway-arrow-color;
        }
        [highway = 'living_street'] {
          marker-fill: @living-street-oneway-arrow-color;
        }
        [highway = 'pedestrian'] {
          marker-fill: @pedestrian-oneway-arrow-color;
        }
        [highway = 'raceway'] {
          marker-fill: @raceway-oneway-arrow-color;
        }
      }
    }
  }
}

#railways-text-name {
  /* Mostly started from z17. */
  [railway = 'rail'],
  [railway = 'subway'],
  [railway = 'narrow_gauge'],
  [railway = 'light_rail'],
  [railway = 'preserved'],
  [railway = 'funicular'],
  [railway = 'monorail'],
  [railway = 'tram'] {
    [zoom >= 17] {
      text-name: "[name]";
      text-fill: #666666;
      text-size: 10;
      text-dy: 6;
      text-spacing: 900;
      text-clip: false;
      text-placement: line;
      text-face-name: @book-fonts;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
      text-repeat-distance: @railway-text-repeat-distance;
    }
    [zoom >= 19] {
      text-size: 11;
      text-dy: 7;
    }
  }
  [railway = 'rail'] {
    /* Render highspeed rails from z11,
       other main routes at z14. */
    [highspeed = 'yes'] {
      [zoom >= 11] {
        text-name: "[name]";
        text-fill: #666666;
        text-size: 10;
        text-dy: 3;
        text-spacing: 300;
        text-clip: false;
        text-placement: line;
        text-face-name: @book-fonts;
        text-halo-radius: @standard-halo-radius;
        text-halo-fill: @standard-halo-fill;
        text-repeat-distance: @railway-text-repeat-distance;
      }
      [zoom >= 13] {
        text-dy: 6;
      }
      [zoom >= 14] {
        text-spacing: 600;
      }
      [zoom >= 17] {
        text-size: 11;
        text-dy: 7;
      }
      [zoom >= 19] {
        text-size: 12;
        text-dy: 8;
      }
    }
    [highspeed != 'yes'][usage = 'main'] {
      [zoom >= 14] {
        text-name: "[name]";
        text-fill: #666666;
        text-size: 10;
        text-dy: 6;
        text-spacing: 300;
        text-clip: false;
        text-placement: line;
        text-face-name: @book-fonts;
        text-halo-radius: @standard-halo-radius;
        text-halo-fill: @standard-halo-fill;
        text-repeat-distance: @railway-text-repeat-distance;
      }
      [zoom >= 17] {
        text-spacing: 600;
        text-size: 11;
        text-dy: 7;
      }
      [zoom >= 19] {
        text-size: 12;
        text-dy: 8;
      }
    }
  }
  /* Other minor railway styles. For service rails, see:
     https://github.com/gravitystorm/openstreetmap-carto/pull/2687 */
  [railway = 'preserved'],
  [railway = 'miniature'],
  [railway = 'disused'],
  [railway = 'construction'] {
    [zoom >= 17] {
      text-name: "[name]";
      text-fill: #666666;
      text-size: 10;
      text-dy: 6;
      text-spacing: 900;
      text-clip: false;
      text-placement: line;
      text-face-name: @book-fonts;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
      text-repeat-distance: @railway-text-repeat-distance;
    }
    [zoom >= 19] {
      text-size: 11;
      text-dy: 7;
    }
  }
}