# Public Transit Focus Tile Server

This project is a customization of [OpenStreetMap-Carto](https://github.com/gravitystorm/openstreetmap-carto) (which is of course an indispensable reference for this project). The Public Transit Focus Tile Server is essentially a collection of customized CartoCSS stylesheets for styling [web map tiles](https://en.wikipedia.org/wiki/Tiled_web_map), and a stack of containerized software to be able to deploy the tile server -- at least as a proof-of-concept. The styles chosen for this project are intended to be used in web maps where the presentation of fixed-route public transit is a major focus.

The general purpose, the cartographic design goals, and guidelines for this style are outlined in [CARTOGRAPHY.md](CARTOGRAPHY.md).

The CartoCSS stylesheets are intended to be modified and work with [Kosmtik](https://github.com/kosmtik/kosmtik) and also with the command-line [CartoCSS](https://github.com/mapbox/carto) processor.

# Installation

You need a PostGIS database populated with OpenStreetMap data along with auxillary shapefiles.
See [INSTALL.md](INSTALL.md).

# Versioning

For now, this project is an isolated modification of the upstream [OpenStreetMap-Carto](https://github.com/gravitystorm/openstreetmap-carto). The primary modifications used for this project primarily involve style, and will not likely involve dependency - even with even major upstream changes. However, it is nonetheless important for developers to understand that this project is downstream of (but disconnected from) OpenStreetMap-Carto, and that there may someday be a need to reconcile this project with changes that occur upstream -- reconciliation which, as of this writing, has not in any way been arranged in an automated way.

# Resources

Valuable resources for using/understanding this project:

* [Overpass-turbo](https://overpass-turbo.eu/) - Overpass is a web-based filtering tool for OpenStreetMap. With overpass turbo you can run Overpass API queries and analyse the resulting OpenStreetMap data interactively on a map. This is very valuable for quickly testing and choosing which data to include/excluse from presentation on tiles (without needing to reload databases/web servers). This tool was used extensively in realizing this project
* [Bounding Box Tool](https://boundingbox.klokantech.com/) - This is a very handy tool for quickly assigning a bounding box with a GUI, and getting its coordinates for use in a stylesheet. This tool was used to quickly determine which bounding box to start with at this map's intended maximum zoom level.
* [My own map: Rendering techniques and software](https://www.youtube.com/watch?v=vQpoMRnMJe8) - Youtube video with very valuable context on how to make your own map tiles
* [OpenStreetMap-Carto](https://github.com/gravitystorm/openstreetmap-carto) - Of course this project is based on OpenStreetMap-Carto.
* [Extracting Geodata from OpenStreetMap with osmfilter](https://journocode.com/en/tutorials/extracting-geodata-from-openstreetmap-with-osmfilter) - This article contains crucial information for understanding how to acquire OpenStreetMap data, and "filter out" unneeded features (Overpass helps with this) so it is ready to load into a PostGIS database.
